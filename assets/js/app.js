/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// import '../scss/app.scss';
import '../scss/contact.scss';
import '../scss/divider-text-middle.scss';
import '../scss/features-clean.scss';
import '../scss/footer.scss';
import '../scss/home-page.scss';
import '../scss/lightbox-gallery.scss';
import '../scss/navigation.scss';
import '../scss/text-box.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
