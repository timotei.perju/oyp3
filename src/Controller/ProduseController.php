<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProduseController extends AbstractController
{
    /**
     * @Route("/produse", name="produse")
     */
    public function index()
    {
        return $this->render('produse/index.html.twig', [
            'controller_name' => 'ProduseController',
        ]);
    }
}
