<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MaterialePlasticeController extends AbstractController
{
    /**
     * @Route("/materiale/plastice", name="materiale_plastice")
     */
    public function index()
    {
        return $this->render('materiale_plastice/index.html.twig', [
            'controller_name' => 'MaterialePlasticeController',
        ]);
    }
}
