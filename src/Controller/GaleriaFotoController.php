<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GaleriaFotoController extends AbstractController
{
    /**
     * @Route("/galeria/foto", name="galeria_foto")
     */
    public function index()
    {
        return $this->render('galeria_foto/index.html.twig', [
            'controller_name' => 'GaleriaFotoController',
        ]);
    }
}
