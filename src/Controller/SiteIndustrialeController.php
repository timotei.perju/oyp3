<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteIndustrialeController extends AbstractController
{
    /**
     * @Route("/site/industriale", name="site_industriale")
     */
    public function index()
    {
        return $this->render('site_industriale/index.html.twig', [
            'controller_name' => 'SiteIndustrialeController',
        ]);
    }
}
