<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SiteFantaController extends AbstractController
{
    /**
     * @Route("/site/fanta", name="site_fanta")
     */
    public function index()
    {
        return $this->render('site_fanta/index.html.twig', [
            'controller_name' => 'SiteFantaController',
        ]);
    }
}
